package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

// Copied from - https://github.com/faiface/beep/wiki/Composing-and-controlling#Ctrl
func main() {
	// Read in file
	f, err := os.Open("Computer_Music_All-stars_-_Energy_Fix.mp3")
	if err != nil {
		log.Fatal(err)
	}
	
	// Initialize streamer and format
	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}
	// Auto-close streamer when done
	defer streamer.Close()
	
	// Initialize speaker system
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	
	// Play streamer on speaker
	// Allow "Enter" to pause/resume playback
	ctrl := &beep.Ctrl{Streamer: streamer, Paused: false}
	speaker.Play(ctrl)
	
	for {
		fmt.Print("Press [ENTER] to pause/resume.")
		fmt.Scanln()
		
		speaker.Lock()
		ctrl.Paused = !ctrl.Paused
		speaker.Unlock()
	}
}