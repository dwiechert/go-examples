package main

import (
    "fmt"
	"os"
	
	"bitbucket.org/dwiechert/go-examples/db/kvs"
)

func main() {
	store, err := kvs.Open("C:\\bolt.db")
	if err != nil {
		fmt.Println("Error opening db.")
		fmt.Println(err)
		os.Exit(1)
	}
	
	err = store.Put("key1", "value1")
	if err != nil {
		fmt.Println("Error putting key/value.")
		fmt.Println(err)
		os.Exit(2)
	}
	
	m := map[string]int {
		"s1": 100,
		"s2": 200,
	}
	err = store.Put("key2", m)
	if err != nil {
		fmt.Println("Error putting key/value.")
		fmt.Println(err)
		os.Exit(2)
	}
	
	var value string
	err = store.Get("key1", &value)
	if err != nil {
		fmt.Println("Error getting key/value.")
		fmt.Println(err)
		os.Exit(3)
	}
	
	fmt.Printf("Read value1: %s\n", value)
	
	var readM map[string]int
	err = store.Get("key2", &readM)
	if err != nil {
		fmt.Println("Error getting key/value.")
		fmt.Println(err)
		os.Exit(3)
	}
	
	fmt.Printf("Read value2: %s\n", readM)
	
	err = store.Delete("key1")
	if err != nil {
		fmt.Println("Error deleting key/value.")
		fmt.Println(err)
		os.Exit(4)
	}
	
	err = store.Get("key1", &value)
	if err != nil {
		fmt.Println("Error getting key/value.")
		fmt.Println(err)
	}
	
	store.Close()
}