package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Dir struct {
	Files []string
}

func main() {
	fileInfo, err := ioutil.ReadDir("./")
	if err != nil {
		os.Exit(1)
	}
	
	var files []string
	for _, f := range fileInfo {
		fmt.Println(f.Name())
		if strings.HasSuffix(f.Name(), ".go") {
			files = append(files, f.Name())
		}
	}
	
	d := Dir{files}
	fmt.Println(d)
}
