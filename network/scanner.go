package main

import (
	"log"
	"net"
)

// Help from:
// https://github.com/timest/goscan/blob/master/src/main/main.go
// https://golang.org/pkg/net/
func main() {
	interfaces, _ := net.Interfaces()
	
	for _, it := range interfaces {
		addr, _ := it.Addrs()
		for _, a := range addr {
			//log.Printf("%v", a)
			if ip, ok := a.(*net.IPNet); ok && !ip.IP.IsLoopback() {
				if ip.IP.To4() != nil {
					log.Printf("%s - %s - %s", ip, it.HardwareAddr, it.Name)
				}
			}
		}
	}
}