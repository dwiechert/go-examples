package main

import (
	"bufio"
	"io"
	"log"
	"net"
	"os"
)

const (
	MessageEnd = '\n'
	Port = ":61000"
)

// Start server on address localhost:61000
func Start() error {
	listener, err := net.Listen("tcp", Port)
	
	if err != nil {
		log.Fatal("Unable to listen.", err)
		os.Exit(1)
	}
	
	log.Println("Listening on %s", listener.Addr().String())
	
	for {
		log.Println("Accepting connections.")
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Unable to get connection.", err)
			os.Exit(1)
		}
		
		go HandleMessages(conn)
	}
}

// Listen to a connection and print out messages
func HandleMessages(conn net.Conn) {
	reader := bufio.NewReader(conn)
	defer conn.Close()
	
	for {
		msg, err := reader.ReadString(MessageEnd)
		switch {
			case err == io.EOF:
				return
			case err != nil:
				log.Fatal("Error reading message.", err)
				return
		}
		
		log.Println(msg)
		if err != nil {
			log.Println("Flush failed.", err)
		}
	}
}

// Help from:
// https://github.com/AppliedGo/networking/blob/master/networking.go
// https://golang.org/pkg/net/
// https://golang.org/pkg/bufio/
// https://golang.org/pkg/io/
func main() {	
	err := Start()
	
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("Server setup")
}