package main

import (
	"bufio"
	"flag"
	"log"
	"net"
	"os"
)

const (
	MessageEnd = "\n"
	Port = ":61000"
)

// Connect to server on port 61000
func Connect(addr string) net.Conn {
	log.Printf("Connecting to: %s", addr)
	conn, err := net.Dial("tcp", addr + Port)
	if err != nil {
		log.Fatal("Error connecting.", err)
		os.Exit(1)
	}
	
	return conn
}

// Send message to connection
func SendMessage(message string, conn net.Conn) {
	writer := bufio.NewWriter(conn)
	defer conn.Close()
	
	_, err := writer.WriteString(message + MessageEnd)
	if err != nil {
		log.Fatal("Error writing data.", err)
		os.Exit(1)
	}
	
	err = writer.Flush()
	if err != nil {
		log.Println("Flush failed.", err)
	}
}

// Help from:
// https://github.com/AppliedGo/networking/blob/master/networking.go
// https://golang.org/pkg/net/
// https://golang.org/pkg/bufio/
// https://golang.org/pkg/io/
func main() {
	ip := flag.String("ip", "", "IP to connect to.")
	msg := flag.String("msg", "", "Message to send.")
	flag.Parse()
	
	conn := Connect(*ip)
	SendMessage(*msg, conn)
	
	log.Println("Client done.")
}