package main

import (
	"fmt"
	"log"
	"io/ioutil"
	"os"
	"strings"
	
	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"fyne.io/fyne/app"
	"fyne.io/fyne/layout"
)

type Data struct {
	DirName string
	Files []string
	DirInput *widget.Entry
	Songs *widget.Select
}

func (d *Data) Load() {
	fileInfo, err := ioutil.ReadDir(d.DirInput.Text)
	if err != nil {
		log.Fatal("Error opening directory.", err)
		os.Exit(1)
	}
	
	d.DirName = d.DirInput.Text
	
	var files []string
	for _, f := range fileInfo {
		if strings.HasSuffix(f.Name(), ".go") {
			files = append(files, f.Name())
		}
	}
	
	d.Files = files
	fmt.Println(d.Files)
	d.Songs.Options = d.Files
}

func Play(song string) {
	fmt.Println(song)
}

func main() {
	app := app.New()
	
	window := app.NewWindow("UI Listing")
	
	window.SetMainMenu(fyne.NewMainMenu(fyne.NewMenu("Open", fyne.NewMenuItem("Open", func() { fmt.Println("Open clicked.") }))))
	window.SetMaster()
	
	// Create reference so we can modify the values
	data := &Data{}
	data.DirName = "                                 "
	data.DirInput = widget.NewEntry()
	data.Songs = widget.NewSelect([]string {}, func(song string) {
		fmt.Println(song)
	})
	
	loadButton := widget.NewButton("Load", func() {
		data.Load()
	})
	
	window.SetContent(
		fyne.NewContainerWithLayout(
			layout.NewGridLayout(3),
			widget.NewLabel("Directory: "),
			data.DirInput,
			loadButton,
		fyne.NewContainerWithLayout(
			layout.NewGridLayout(1),
			data.Songs,
			),
		),
	)
	
	window.ShowAndRun()
}