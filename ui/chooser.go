package main

import (
	"fmt"

	"fyne.io/fyne"
	"fyne.io/fyne/widget"
	"fyne.io/fyne/app"
)

// Helpful links:
// https://github.com/fyne-io
// https://github.com/fyne-io/examples
// https://godoc.org/fyne.io/fyne#pkg-subdirectories
// https://godoc.org/fyne.io/fyne/widget
// https://github.com/fyne-io/examples/blob/develop/calculator/main.go
// https://github.com/fyne-io/fyne/blob/master/cmd/fyne_demo/screens/widget.go
// https://fyne.io/

type data struct {
	output *widget.Label
	input *widget.Entry
}

func (d *data) play() {
	d.output.SetText(d.input.Text)
}

func main() {
	app := app.New()

	w := app.NewWindow("Music Player")
	
	w.SetMainMenu(fyne.NewMainMenu(fyne.NewMenu("Open", fyne.NewMenuItem("Open", func() { fmt.Println("Open clicked.") }))))
	w.SetMaster()
	
	data := &data{}
	
	data.output = widget.NewLabel("")
	data.input = widget.NewEntry()
	
	//playFunc := play(nameLabel, songEntry)
	playButton := widget.NewButton("Play", func() {
		fmt.Println("Play button pressed.")
		data.play()
		//nameLabel.SetText(songEntry.Text)
	})
	
	w.SetContent(widget.NewHBox(
		widget.NewLabel("Song Name: "),
		data.output,
		data.input,
		playButton,
	))

	w.ShowAndRun()
}